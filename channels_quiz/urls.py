"""channels_quiz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import include, url
from django.views.i18n import JavaScriptCatalog


from channels_quiz.views import main_page
from quiz.views import QuizGrid, QuestionGrid, VariantGrid, QuizQuestionGrid

urlpatterns = [
    path('admin/', admin.site.urls),
]

# Allauth views.
if settings.ALLAUTH_DJK_URLS:
    # More pretty-looking bootstrap forms but possibly are not compatible with arbitrary allauth version:
    urlpatterns.append(
        url(r'^accounts/', include('django_jinja_knockout._allauth.urls'))
    )
else:
    # Standard allauth DTL templates working with Jinja2 templates via {% load jinja %} template tag library.
    urlpatterns.append(
        url(r'^accounts/', include('allauth.urls'))
    )

js_info_dict = {
    'domain': 'djangojs',
    'packages': ('channels_quiz',),
}

urlpatterns += [
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(**js_info_dict), name='javascript-catalog'),
    url(r'^$', main_page, name='club_main_page',
        kwargs={'view_title': 'Main page', 'allow_anonymous': True}),
    url(r'^quiz-grid(?P<action>/?\w*)/$', QuizGrid.as_view(),
        name='quiz_grid',
        kwargs={'view_title': 'Тесты'}),
    url(r'^question-grid(?P<action>/?\w*)/$', QuestionGrid.as_view(),
        name='question_grid',
        kwargs={'view_title': 'Вопросы'}),
    url(r'^variant-grid(?P<action>/?\w*)/$', VariantGrid.as_view(),
        name='variant_grid',
        kwargs={'view_title': 'Варианты ответов'}),
    url(r'^quiz-question-grid(?P<action>/?\w*)/$', QuizQuestionGrid.as_view(),
        name='quiz_question_grid',
        kwargs={'view_title': 'Вопросы тестов'}),
]
