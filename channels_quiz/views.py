from django.shortcuts import render


def main_page(request, **kwargs):
    if request.method == 'GET':
        return render(request, 'main.html')
