from django.shortcuts import render

from django_jinja_knockout.views import KoGridView

from .forms import QuizForm, QuestionForm, VariantForm, QuizQuestionForm


class QuizGrid(KoGridView):

    form = QuizForm
    grid_fields = ['description']
    allowed_sort_orders = grid_fields


class QuestionGrid(KoGridView):

    form = QuestionForm
    grid_fields = ['text', 'max_time']
    allowed_sort_orders = grid_fields


class VariantGrid(KoGridView):
    client_routes = {
        'question_grid'
    }
    form = VariantForm
    grid_fields = [
        'question__text', 'text', 'is_correct'
    ]
    allowed_sort_orders = grid_fields


class QuizQuestionGrid(KoGridView):
    client_routes = {
        'quiz_grid', 'question_grid'
    }
    form = QuizQuestionForm
    grid_fields = [
        'quiz__description', 'question__text', 'question__max_time', 'next'
    ]
