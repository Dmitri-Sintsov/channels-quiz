from django_jinja_knockout.forms import BootstrapModelForm
from django_jinja_knockout.widgets import ForeignKeyGridWidget

from .models import Quiz, Question, Variant, QuizQuestion


class QuizForm(BootstrapModelForm):

    class Meta:
        model = Quiz
        fields = '__all__'


class QuestionForm(BootstrapModelForm):

    class Meta:
        model = Question
        fields = '__all__'


class VariantForm(BootstrapModelForm):

    class Meta:
        model = Variant
        fields = '__all__'
        widgets = {
            'question': ForeignKeyGridWidget(model=Question, grid_options={
                'pageRoute': 'question_grid',
            }),
        }


class QuizQuestionForm(BootstrapModelForm):

    class Meta:
        model = QuizQuestion
        fields = '__all__'
        widgets = {
            'quiz': ForeignKeyGridWidget(model=Quiz, grid_options={
                'pageRoute': 'quiz_grid',
            }),
            'question': ForeignKeyGridWidget(model=Question, grid_options={
                'pageRoute': 'question_grid',
            }),
            'next': ForeignKeyGridWidget(model=QuizQuestion, grid_options={
                'pageRoute': 'quiz_question_grid',
            }),
        }

    def full_clean(self):
        # Make self.data mutable.
        self.data = self.data.copy()
        # Allow empty value of foreign key (by default it is disallowed).
        if 'next' in self.data and self.data['next'] == '0':
            self.data['next'] = None
        super().full_clean()
