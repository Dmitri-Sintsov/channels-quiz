from collections import OrderedDict
from django.db import models

from django_jinja_knockout.tpl import limitstr, str_dict


class Quiz(models.Model):

    description = models.TextField('Описание', unique=True, max_length=4096)

    class Meta:
        verbose_name = 'Тест'
        verbose_name_plural = 'Тесты'
        ordering = ['description']

    def get_str_fields(self):
        return OrderedDict(
            description=limitstr(self.description, maxlen=255)
        )

    def __str__(self):
        return str_dict(self.get_str_fields())


class Question(models.Model):

    text = models.TextField('Вопрос', unique=True, max_length=4096)
    max_time = models.PositiveIntegerField('Время на ответ в секундах', default=60)

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
        ordering = ['text']

    def get_str_fields(self):
        return OrderedDict(
            text=limitstr(self.text, maxlen=255),
            max_time=str(self.max_time)
        )

    def __str__(self):
        return str_dict(self.get_str_fields())


class Variant(models.Model):

    question = models.ForeignKey(Question, on_delete=models.CASCADE, verbose_name='Вопрос')
    text = models.TextField('Текст ответа', max_length=4096)
    is_correct = models.BooleanField('Корректный ответ')

    class Meta:
        verbose_name = 'Вариант ответа'
        verbose_name_plural = 'Варианты ответов'
        unique_together = [
            ['text', 'question'],
            ['question', 'text', 'is_correct'],
        ]
        ordering = ['text', 'question']


class QuizQuestion(models.Model):

    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, verbose_name='Тест')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, verbose_name='Вопрос')
    next = models.ForeignKey(
        'self', blank=True, null=True, default=None, on_delete=models.CASCADE, verbose_name='Следующий вопрос'
    )

    class Meta:
        verbose_name = 'Вопрос теста'
        verbose_name_plural = 'Вопросы тестов'
        unique_together = [
            ['quiz', 'question'],
            ['quiz', 'next']
        ]
        ordering = [
            'quiz__description', 'next__question__text'
        ]

    def get_str_fields_no_recursion(self):
        return OrderedDict(
            quiz=self.quiz.get_str_fields(),
            question=self.question.get_str_fields(),
        )

    def get_str_fields(self):
        str_fields = self.get_str_fields_no_recursion()
        if self.next is not None:
            str_fields['next'] = self.next.get_str_fields_no_recursion()
        return str_fields

    def __str__(self):
        return str_dict(self.get_str_fields())
