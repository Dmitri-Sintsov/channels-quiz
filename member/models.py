from django.db import models
from django.contrib.auth.models import User

from quiz.models import Quiz, Variant


class QuizGroup(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Группа опрашиваемых'
        verbose_name_plural = 'Группы опрашиваемых'
        unique_together = ['user', 'quiz']


class MemberAnswer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    variant = models.ForeignKey(Variant, on_delete=models.CASCADE)
    time_spent = models.PositiveIntegerField('Время на ответ в секундах')

    class Meta:
        verbose_name = 'Выбранный ответ'
        verbose_name_plural = 'Выбранные ответы'
        unique_together = [
            ['user', 'variant'],
            ['user', 'variant', 'time_spent']
        ]
